[![Waffle.io - Issues in progress](https://badge.waffle.io/ryan27968/Taskmaster.png?label=in%20progress&title=In%20Progress)](http://waffle.io/ryan27968/Taskmaster)
#   Taskmaster
Job/program control system written in D.

##  Taskmasterd/Taskmasterctl?
Taskmaster is a combination of two programs. Taskmasterd manages the programs/jobs themselves, and taskmasterctl allows you to control them.

##	Documentation
 - [Taskmasterd Configuration](doc/Taskmasterd%20Config.md)
 - [Taskmasterctl Usage](doc/Taskmasterctl%20Usage.md)
 - [Job Configuration](doc/Job%20Config.md)
 - [TCP communication](doc/Network.md)

For a preconfigured taskmasterd installation example, check out the examples directory.